use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{newline, not_line_ending, space0};
use nom::combinator::{eof, map, opt, peek};
use nom::error::ParseError;
use nom::multi::many0;
use nom::sequence::{pair, preceded, terminated};
use nom::IResult;

use super::directive::{directive, Directive};
use super::word::word;
use super::{alt_dis, discard, tag_sep, Input};

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Item<I> {
    Label(I),
    Directive(Directive<I>),
    LineComment(I),
}

impl<I> From<Directive<I>> for Item<I> {
    fn from(d: Directive<I>) -> Self {
        Self::Directive(d)
    }
}

fn item_label<I: Input, E: ParseError<I>>(input: I) -> IResult<I, Item<I>, E> {
    map(
        terminated(word, preceded(space0, tag_sep(":"))),
        Item::Label,
    )(input)
}

fn item_directive<I: Input, E: ParseError<I>>(input: I) -> IResult<I, Item<I>, E> {
    map(directive, Item::Directive)(input)
}

fn item_comment<I: Input, E: ParseError<I>>(input: I) -> IResult<I, Item<I>, E> {
    map(
        preceded(
            pair(tag("//"), opt(tag(" "))),
            terminated(not_line_ending, alt_dis(newline, eof)),
        ),
        Item::LineComment,
    )(input)
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Program<I> {
    pub items: Vec<Item<I>>,
}

pub fn item_sep<I: Input, E: ParseError<I>>(input: I) -> IResult<I, (), E> {
    alt((discard(tag_sep("\n")), discard(tag_sep(";"))))(input)
}

pub fn program<I: Input, E: ParseError<I>>(input: I) -> IResult<I, Program<I>, E> {
    let (rest, p) = preceded(
        pair(space0, many0(item_sep)),
        many0(terminated(
            alt((
                item_label,
                item_comment,
                terminated(
                    item_directive,
                    alt((
                        discard(item_sep),
                        discard(preceded(space0, peek(tag("//")))),
                        discard(preceded(space0, eof)),
                    )),
                ),
            )),
            many0(item_sep),
        )),
    )(input)?;

    Ok((rest, Program { items: p }))
}

#[allow(unused_macros)]
macro_rules! prog {
    ($($item: expr);* $(;)?) => {
        crate::parser::program::Program {
            items: vec![$($item.into()),*],
        }
    };
}

#[allow(unused_imports)]
pub(crate) use prog;

#[cfg(test)]
mod tests {
    use crate::parser::directive::dir;
    use crate::parser::program::{item_directive, program};
    use crate::parser::test_utils::assert_parse;
    use crate::parser::DirectiveArg;

    use super::{item_comment, item_label, Item};

    #[test]
    fn test_label() {
        assert_parse(Item::Label("foo"), item_label, "foo:");
        assert_parse(Item::Label("foo"), item_label, "foo :");
    }

    #[test]
    fn test_directive() {
        assert_parse(dir!(add), item_directive, "add");
        assert_parse(dir!(add s0), item_directive, "add s0");
        assert_parse(dir!(add s0, 321), item_directive, "add s0, 321");
    }

    #[test]
    fn test_comment() {
        assert_parse(Item::LineComment("foo"), item_comment, "// foo\n");
        assert_parse(Item::LineComment("foo"), item_comment, "//foo\n");
        assert_parse(Item::LineComment(" foo"), item_comment, "//  foo\n");

        assert_parse(Item::LineComment("foo"), item_comment, "// foo");
        assert_parse(Item::LineComment("foo"), item_comment, "//foo");
        assert_parse(Item::LineComment(" foo"), item_comment, "//  foo");
    }

    #[test]
    fn test_program() {
        assert_parse(prog! {}, program, "");
        assert_parse(prog! {}, program, "\n");
        assert_parse(prog! {}, program, ";");
        assert_parse(prog! {}, program, " ");

        assert_parse(
            prog! { Item::LineComment("Hello World"); },
            program,
            "// Hello World",
        );

        assert_parse(prog! { Item::Label("foo"); }, program, "foo:");

        assert_parse(prog! { dir!(add); }, program, "add;");
        assert_parse(prog! { dir!(add s0); }, program, "add s0;");
        assert_parse(prog! { dir!(add s0, s1); }, program, "add s0, s1;");

        assert_parse(prog! { dir!(add); }, program, "add");
        assert_parse(prog! { dir!(add s0); }, program, "add s0");
        assert_parse(prog! { dir!(add s0, s1); }, program, "add s0, s1");

        assert_parse(
            prog! {
                dir!(".section", text);
                Item::Label("fib");
                dir!(li t1, 1);
                Item::Label("_loop.enter");
                dir!("mul.w", t1, t1, a0);
                dir!(addi a0, a0, (-1));
                dir!(jnez 10, (DirectiveArg::Word("_loop.enter")));
                Item::LineComment("this is horrible formatting");
                dir!(ret);
            },
            program,
            r#"
.section text

fib:
    li t1, 1
    _loop.enter:
        mul.w t1, t1, a0
        addi a0, a0, -1; jnez 10, _loop.enter // this is horrible formatting
ret
"#,
        )
    }
}
