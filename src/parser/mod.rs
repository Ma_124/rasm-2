#![allow(dead_code)]

mod directive;
mod int;
#[cfg(feature = "nom_locate")]
mod locate;
mod program;
#[cfg(feature = "nom-supreme")]
mod supreme;
mod word;

pub use directive::{Directive, DirectiveArg};
pub use int::Integer;
pub use program::{Item, Program};

use std::ops::{Range, RangeFrom, RangeTo};

use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::space0;
use nom::error::ParseError;
use nom::sequence::{preceded, terminated};
use nom::{
    AsBytes, AsChar, Compare, IResult, InputIter, InputLength, InputTake, InputTakeAtPosition,
    Offset, Parser, Slice,
};

#[cfg(feature = "nom_locate")]
pub use locate::*;

#[allow(unused_imports)]
pub(crate) use directive::{dir, dir_arg};

pub trait InputCond {
    type Item: AsChar + Clone;

    fn as_str(&self) -> &str;
}

impl<'a> InputCond for &'a str {
    type Item = char;

    fn as_str(&self) -> &str {
        self
    }
}

pub trait Input:
    InputCond
    + AsBytes
    + Clone
    + Compare<&'static str>
    + InputIter<Item = <Self as InputCond>::Item>
    + InputLength
    + InputTake
    + InputTakeAtPosition<Item = <Self as InputCond>::Item>
    + Offset
    + Slice<Range<usize>>
    + Slice<RangeTo<usize>>
    + Slice<RangeFrom<usize>>
{
}

impl<T> Input for T where
    T: InputCond
        + AsBytes
        + Clone
        + Compare<&'static str>
        + InputIter<Item = <Self as InputCond>::Item>
        + InputLength
        + InputTake
        + InputTakeAtPosition<Item = <Self as InputCond>::Item>
        + Offset
        + Slice<Range<usize>>
        + Slice<RangeTo<usize>>
        + Slice<RangeFrom<usize>>
{
}

pub(crate) fn tag_sep<I: Input, E: ParseError<I>>(s: &'static str) -> impl Parser<I, I, E> {
    move |input| preceded(space0, terminated(tag(s), space0))(input)
}

pub(crate) fn discard<I: Clone, O, E: ParseError<I>, F>(
    mut f: F,
) -> impl FnMut(I) -> IResult<I, (), E>
where
    F: Parser<I, O, E>,
{
    move |input: I| {
        let (rest, _) = f.parse(input)?;
        Ok((rest, ()))
    }
}

pub(crate) fn alt_dis<I: Input, E: ParseError<I>, O1, O2>(
    a: impl Parser<I, O1, E> + Clone,
    b: impl Parser<I, O2, E> + Clone,
) -> impl Parser<I, (), E> {
    move |input| alt((discard(a.clone()), discard(b.clone())))(input)
}

#[cfg(test)]
mod test_utils {
    use std::fmt::Debug;

    use crate::color;
    use nom::Parser;

    #[cfg(feature = "nom-supreme")]
    pub use crate::parser::supreme::Err;

    #[cfg(not(feature = "nom-supreme"))]
    pub type Err<'a> = nom::error::Error<&'a str>;

    pub fn assert_partial_parse<'a, T: Eq + Debug>(
        want_rest: &str,
        want: impl Into<T>,
        mut parser: impl Parser<&'a str, T, Err<'a>>,
        input: &'a str,
    ) {
        match parser.parse(input) {
            Ok(got) => {
                assert_eq!(
                    (want_rest, want.into()),
                    (got.0, got.1),
                    "input = `{}`",
                    color::error(input)
                );
            }
            Err(e) => println!("Parse error: {:#?}\ninput = `{}`", e, color::error(input)),
        }
    }

    pub fn assert_parse<'a, T: Eq + Debug>(
        want: impl Into<T>,
        parser: impl Parser<&'a str, T, Err<'a>>,
        input: &'a str,
    ) {
        assert_partial_parse("", want, parser, input);
    }
}
