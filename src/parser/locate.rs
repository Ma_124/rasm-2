use nom_locate::LocatedSpan;

use super::InputCond;

#[derive(Debug, Clone, Copy)]
pub struct LocationExtra<'a> {
    file_name: &'a str,
}

pub type Span<'a> = LocatedSpan<&'a str, LocationExtra<'a>>;

pub fn new_file<'a>(text: &'a str, file_name: &'a str) -> Span<'a> {
    Span::new_extra(text, LocationExtra { file_name })
}

impl<'a> InputCond for Span<'a> {
    type Item = char;

    fn as_str(&self) -> &str {
        self.fragment()
    }
}

#[cfg(test)]
mod tests {
    use nom::error::Error;

    use crate::parser::directive::directive;
    use crate::parser::{InputCond, Integer};

    use super::new_file;

    #[test]
    fn test_nom_locate() {
        let (rest, dir) = directive::<_, Error<_>>(new_file("add a0, 1", "test.s")).unwrap();
        assert_eq!("", rest.as_str());

        assert_eq!("add", dir.name.as_str());
        assert_eq!(2, dir.args.len());
        assert_eq!("a0", dir.args[0].word().unwrap().as_str());
        assert_eq!(Integer::from(1), dir.args[1].int().unwrap());

        assert_eq!(0, dir.name.location_offset());
        assert_eq!(1, dir.name.location_line());
        assert_eq!(1, dir.name.get_column());

        assert_eq!(4, dir.args[0].word().unwrap().location_offset());
        assert_eq!(1, dir.args[0].word().unwrap().location_line());
        assert_eq!(5, dir.args[0].word().unwrap().get_column());
    }
}
