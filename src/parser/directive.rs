use nom::branch::alt;
use nom::character::complete::space1;
use nom::combinator::{map, opt};
use nom::error::ParseError;
use nom::multi::separated_list0;
use nom::sequence::preceded;
use nom::IResult;

use crate::parser::tag_sep;

use super::int::{int, Integer};
use super::word::word;
use super::Input;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum DirectiveArg<I> {
    Word(I),
    Int(Integer),
}

impl<I: Clone> DirectiveArg<I> {
    pub fn word(&self) -> Option<I> {
        match self {
            DirectiveArg::Word(w) => Some(w.clone()),
            DirectiveArg::Int(_) => None,
        }
    }
}

impl<I> DirectiveArg<I> {
    pub fn int(&self) -> Option<Integer> {
        match self {
            DirectiveArg::Word(_) => None,
            DirectiveArg::Int(i) => Some(*i),
        }
    }
}

impl<'a> From<i32> for DirectiveArg<&'a str> {
    fn from(i: i32) -> Self {
        Self::Int(i.into())
    }
}

pub fn directive_arg<I: Input, E: ParseError<I>>(input: I) -> IResult<I, DirectiveArg<I>, E> {
    alt((map(word, DirectiveArg::Word), map(int, DirectiveArg::Int)))(input)
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Directive<I> {
    pub name: I,
    pub args: Vec<DirectiveArg<I>>,
}

#[allow(unused_macros)]
macro_rules! dir {
    ($name: ident $($arg: tt),*) => {
        crate::parser::dir!(stringify!($name), $($arg),*)
    };
    ($name: expr, $($arg: tt),*) => {
        crate::parser::Directive {
            name: $name,
            args: vec![
                $(crate::parser::dir_arg!($arg)),*
            ],
        }
    };
}

#[allow(unused_imports)]
pub(crate) use dir;

#[allow(unused_macros)]
macro_rules! dir_arg {
    ($word: ident) => {
        crate::parser::DirectiveArg::<&str>::Word(stringify!($word))
    };
    ($e: expr) => {
        crate::parser::DirectiveArg::<&str>::from($e)
    };
}

#[allow(unused_imports)]
pub(crate) use dir_arg;

pub fn directive<I: Input, E: ParseError<I>>(input: I) -> IResult<I, Directive<I>, E> {
    let (rest, name) = word(input)?;
    let (rest, args) = opt(preceded(
        space1,
        separated_list0(tag_sep(","), directive_arg),
    ))(rest)?;
    Ok((
        rest,
        Directive {
            name,
            args: args.unwrap_or_default(),
        },
    ))
}

#[cfg(test)]
mod tests {
    use crate::parser::directive::{directive, directive_arg};
    use crate::parser::test_utils::assert_parse;

    #[test]
    fn test_directive_arg() {
        assert_parse(dir_arg!(foo), directive_arg, "foo");
        assert_parse(dir_arg!(_foo), directive_arg, "_foo");

        assert_parse(dir_arg!(0b101), directive_arg, "0b101");
        assert_parse(dir_arg!(0o123), directive_arg, "0o123");
        assert_parse(dir_arg!(0x123), directive_arg, "0x123");
        assert_parse(dir_arg!(123), directive_arg, "123");
    }

    #[test]
    fn test_directive() {
        // name
        assert_parse(dir!(add), directive, "add");
        assert_parse(dir!(add), directive, "add ");

        // name dir_arg
        assert_parse(dir!(add s0), directive, "add s0");
        assert_parse(dir!(add 1), directive, "add 1");

        // name word, word
        assert_parse(dir!(add s0, s1), directive, "add s0,s1");
        assert_parse(dir!(add s0, s1), directive, "add s0 ,s1");
        assert_parse(dir!(add s0, s1), directive, "add s0, s1");
        assert_parse(dir!(add s0, s1), directive, "add s0 , s1");

        // name int, word
        assert_parse(dir!(add 123, s1), directive, "add 123,s1");
        assert_parse(dir!(add 123, s1), directive, "add 123 ,s1");
        assert_parse(dir!(add 123, s1), directive, "add 123, s1");
        assert_parse(dir!(add 123, s1), directive, "add 123 , s1");

        // name word, int
        assert_parse(dir!(add s0, 456), directive, "add s0,456");
        assert_parse(dir!(add s0, 456), directive, "add s0 ,456");
        assert_parse(dir!(add s0, 456), directive, "add s0, 456");
        assert_parse(dir!(add s0, 456), directive, "add s0 , 456");

        // name int, int
        assert_parse(dir!(add 123, 456), directive, "add 123,456");
        assert_parse(dir!(add 123, 456), directive, "add 123 ,456");
        assert_parse(dir!(add 123, 456), directive, "add 123, 456");
        assert_parse(dir!(add 123, 456), directive, "add 123 , 456");

        // name words...
        assert_parse(dir!(add s0, s1), directive, "add s0, s1");
        assert_parse(dir!(add s0, s1, s2), directive, "add s0, s1, s2");
        assert_parse(dir!(add s0, s1, s2, s3), directive, "add s0, s1, s2, s3");
    }
}
