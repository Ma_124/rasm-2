use std::fmt::{self, Debug, Display, Formatter};

use nom_supreme::final_parser::{Location, RecreateContext};

use crate::color::info;

pub struct ErrorLocation<'a> {
    text: &'a str,
    loc: Location,
}

impl<'a> RecreateContext<&'a str> for ErrorLocation<'a> {
    fn recreate_context(original_input: &'a str, tail: &'a str) -> Self {
        Self {
            text: original_input,
            loc: Location::recreate_context(original_input, tail),
        }
    }
}

impl<'a> Debug for ErrorLocation<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:#}", self)
    }
}

impl<'a> Display for ErrorLocation<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        if f.alternate() {
            writeln!(f)?;
            for (i, line) in self.text.lines().enumerate() {
                write!(f, "{} {}", info(&format!("{} |", i)), line)?;
                if i + 1 == self.loc.line {
                    if self.loc.column > line.len() {
                        write!(f, "↵")?;
                    }
                    write!(f, "\n  | {:>w$}", "^", w = self.loc.column)?;
                }
                writeln!(f)?;
            }
        } else {
            writeln!(f, "{:?} at {}", self.text, self.loc)?;
        }
        Ok(())
    }
}

pub type Err<'a> = nom_supreme::error::ErrorTree<&'a str>;
