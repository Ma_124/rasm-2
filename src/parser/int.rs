use std::convert::TryFrom;

use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{digit1, hex_digit1, oct_digit1};
use nom::combinator::recognize;
use nom::error::ParseError;
use nom::multi::many1;
use nom::{IResult, Parser};

use super::Input;

#[derive(Debug, Clone, Copy)]
pub enum Integer {
    Signed(i64),
    Unsigned(u64),
}

impl Integer {
    pub fn unsigned(&self) -> Option<u64> {
        match self {
            Integer::Signed(s) => {
                if *s >= 0 {
                    Some(*s as u64)
                } else {
                    None
                }
            }
            Integer::Unsigned(u) => Some(*u),
        }
    }
}

impl PartialEq for Integer {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Signed(l), Self::Signed(r)) => l == r,
            (Self::Unsigned(l), Self::Unsigned(r)) => l == r,
            (Self::Signed(l), Self::Unsigned(r)) => u64::try_from(*l).ok() == Some(*r),
            (Self::Unsigned(l), Self::Signed(r)) => u64::try_from(*r).ok() == Some(*l),
        }
    }
}

impl Eq for Integer {}

impl From<i32> for Integer {
    fn from(i: i32) -> Self {
        Self::Signed(i as i64)
    }
}

impl From<i64> for Integer {
    fn from(i: i64) -> Self {
        Self::Signed(i)
    }
}

impl From<u64> for Integer {
    fn from(u: u64) -> Self {
        Self::Unsigned(u)
    }
}

fn int_for_base<I: Input, E: ParseError<I>>(
    prefix: &'static str,
    base: u32,
    mut f: impl Parser<I, I, E>,
) -> impl Parser<I, Integer, E> {
    move |input: I| {
        let (rest, sign) = alt((tag("+"), tag("-"), tag("")))(input)?;
        let (rest, _) = tag(prefix)(rest)?;
        let (rest, digits) = f.parse(rest)?;
        // TODO manual parse handling overflow and _
        let i = if sign.as_str() == "-" {
            Integer::Signed(-i64::from_str_radix(digits.as_str(), base).unwrap())
        } else {
            Integer::Unsigned(u64::from_str_radix(digits.as_str(), base).unwrap())
        };
        Ok((rest, i))
    }
}

pub fn int<I: Input, E: ParseError<I>>(input: I) -> IResult<I, Integer, E> {
    alt((
        int_for_base("0b", 2, recognize(many1(alt((tag("0"), tag("1")))))),
        int_for_base("0o", 8, oct_digit1),
        int_for_base("0x", 16, hex_digit1),
        int_for_base("", 10, digit1),
    ))(input)
}

#[cfg(test)]
mod tests {
    use crate::parser::int::Integer;
    use crate::parser::test_utils::{assert_parse, assert_partial_parse};

    use super::int;

    #[test]
    fn test_int() {
        // full +
        assert_parse(123, int, "123");
        assert_parse(0b101, int, "0b101");
        assert_parse(0o123, int, "0o123");
        assert_parse(0xABC, int, "0xABC");
        assert_parse(0xABC, int, "0xabc");

        // partial +
        assert_partial_parse("A", 129, int, "129A");
        assert_partial_parse("2", 0b101, int, "0b1012");
        assert_partial_parse("8", 0o127, int, "0o1278");
        assert_partial_parse("G", 0xABF, int, "0xABFG");
        assert_partial_parse("g", 0xABF, int, "0xabfg");

        // full -
        assert_parse(-123, int, "-123");
        assert_parse(-0b101, int, "-0b101");
        assert_parse(-0o123, int, "-0o123");
        assert_parse(-0xABC, int, "-0xABC");
        assert_parse(-0xABC, int, "-0xabc");

        // partial -
        assert_partial_parse("A", -129, int, "-129A");
        assert_partial_parse("2", -0b101, int, "-0b1012");
        assert_partial_parse("8", -0o127, int, "-0o1278");
        assert_partial_parse("G", -0xABF, int, "-0xABFG");
        assert_partial_parse("g", -0xABF, int, "-0xabfg");
    }

    #[test]
    fn test_int_eq() {
        assert_eq!(Integer::Signed(0), Integer::Signed(0));
        assert_eq!(Integer::Unsigned(0), Integer::Signed(0));
        assert_eq!(Integer::Signed(0), Integer::Unsigned(0));
        assert_eq!(Integer::Unsigned(0), Integer::Unsigned(0));

        assert_eq!(Integer::Signed(123), Integer::Signed(123));
        assert_eq!(Integer::Unsigned(123), Integer::Signed(123));
        assert_eq!(Integer::Signed(123), Integer::Unsigned(123));
        assert_eq!(Integer::Unsigned(123), Integer::Unsigned(123));

        assert_eq!(Integer::from(1i64), Integer::from(1u64));

        assert_eq!(Integer::Signed(i64::MAX), Integer::Signed(i64::MAX));
        assert_eq!(
            Integer::Unsigned(i64::MAX as u64),
            Integer::Signed(i64::MAX)
        );
        assert_eq!(
            Integer::Signed(i64::MAX),
            Integer::Unsigned(i64::MAX as u64)
        );
        assert_eq!(
            Integer::Unsigned(i64::MAX as u64),
            Integer::Unsigned(i64::MAX as u64)
        );

        assert_ne!(Integer::Signed(-1), Integer::Unsigned(1));
        assert_ne!(Integer::Unsigned(1), Integer::Signed(-1));

        assert_ne!(Integer::Signed(-1), Integer::Unsigned(u64::MAX));
        assert_ne!(Integer::Unsigned(u64::MAX), Integer::Signed(-1));

        assert_eq!(Some(0), Integer::Signed(0).unsigned());
        assert_eq!(Some(0), Integer::Unsigned(0).unsigned());

        assert_eq!(Some(100), Integer::Signed(100).unsigned());
        assert_eq!(Some(100), Integer::Unsigned(100).unsigned());

        assert_eq!(None, Integer::Signed(-1).unsigned());
    }
}
