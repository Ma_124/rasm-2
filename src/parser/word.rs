use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{alpha1, digit1};
use nom::combinator::recognize;
use nom::error::ParseError;
use nom::multi::many0;
use nom::sequence::pair;
use nom::IResult;

use super::Input;

fn word_start<I: Input, E: ParseError<I>>(input: I) -> IResult<I, I, E> {
    alt((alpha1, tag("_"), tag(".")))(input)
}

pub fn word<I: Input, E: ParseError<I>>(input: I) -> IResult<I, I, E> {
    recognize(pair(word_start, many0(alt((word_start, digit1)))))(input)
}

#[cfg(test)]
mod tests {
    use crate::parser::test_utils::{assert_parse, assert_partial_parse};

    use super::word;

    #[test]
    fn test_word() {
        // word
        assert_parse("foo", word, "foo");
        assert_parse("foo1", word, "foo1");
        assert_parse("foo_1", word, "foo_1");
        assert_parse("foo.1", word, "foo.1");
        assert_parse(".foo", word, ".foo");
        assert_parse("_foo", word, "_foo");

        // words separated
        assert_partial_parse("-bar", "foo", word, "foo-bar");
        assert_partial_parse("-bar", "foo1", word, "foo1-bar");
        assert_partial_parse("-bar", "foo_1", word, "foo_1-bar");
        assert_partial_parse("-bar", "foo.1", word, "foo.1-bar");
        assert_partial_parse("-bar", ".foo", word, ".foo-bar");
        assert_partial_parse("-bar", "_foo", word, "_foo-bar")
    }
}
