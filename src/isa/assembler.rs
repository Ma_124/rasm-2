use std::collections::HashMap;
use std::io;

use crate::parser::{Directive, Input, Integer};

use super::registers::register_from_str_abi;
use super::{with_bimm, with_iimm, with_rd, with_rs1, with_rs2, write_inst32, AssemblyError};

#[derive(Debug)]
pub struct Assembler<W, I> {
    w: W,
    symbol_table: HashMap<String, u64>,
    relocations: Vec<(u64, Relocation<I>)>,
    offset: u64,
}

#[derive(Debug)]
pub enum Relocation<I> {
    Branch { symbol: I, addend: u64 },
    Relax,
}

impl<W: io::Write, I: Input> Assembler<W, I> {
    pub fn new(w: W) -> Self {
        Self {
            w,
            symbol_table: HashMap::new(),
            relocations: Vec::new(),
            offset: 0,
        }
    }

    #[rustfmt::skip]
    pub fn write_inst(&mut self, dir: Directive<I>) -> Result<(), AssemblyError<I>> {
        match dir.name.as_str() {
            "beq"      => self.branch(dir, 0x0000_0063)?,
            "bne"      => self.branch(dir, 0x0000_1063)?,
            "blt"      => self.branch(dir, 0x0000_4063)?,
            "bge"      => self.branch(dir, 0x0000_5063)?,
            "bltu"     => self.branch(dir, 0x0000_6063)?,
            "bgeu"     => self.branch(dir, 0x0000_7063)?,

            "addi"     => self.reg_imm(dir, 0x0000_0013)?,
            "slti"     => self.reg_imm(dir, 0x0000_2013)?,
            "sltiu"    => self.reg_imm(dir, 0x0000_3013)?,
            "xori"     => self.reg_imm(dir, 0x0000_4013)?,
            "ori"      => self.reg_imm(dir, 0x0000_6013)?,
            "andi"     => self.reg_imm(dir, 0x0000_7013)?,

            "add"      => self.reg(dir, 0x0000_0033)?,
            "sub"      => self.reg(dir, 0x4000_0033)?,
            "sll"      => self.reg(dir, 0x0000_1033)?,
            "slt"      => self.reg(dir, 0x0000_2033)?,
            "sltu"     => self.reg(dir, 0x0000_3033)?,
            "xor"      => self.reg(dir, 0x0000_4033)?,
            "srl"      => self.reg(dir, 0x0000_5033)?,
            "sra"      => self.reg(dir, 0x4000_5033)?,
            "or"       => self.reg(dir, 0x0000_6033)?,
            "and"      => self.reg(dir, 0x0000_7033)?,

            _ => return Err(AssemblyError::UnknownInstruction { dir }),
        };
        Ok(())
    }

    fn branch(&mut self, dir: Directive<I>, op: u32) -> Result<(), AssemblyError<I>> {
        AssemblyError::assert_length(&dir, 3)?;

        let rs1 = self.get_register_at(&dir, 0)?;
        let rs2 = self.get_register_at(&dir, 1)?;
        let addr = self
            .get_address_at(&dir, 2)?
            .map(|addr| addr as i32 - self.offset as i32); // TODO check overflow and bounds

        write_inst32(
            &mut self.w,
            op | with_rs1(rs1) | with_rs2(rs2) | with_bimm(addr.unwrap_or_default() as u32),
        )?;
        self.offset += 4;

        if addr.is_none() {
            todo!("emit relocations")
        }

        Ok(())
    }

    fn reg(&mut self, dir: Directive<I>, op: u32) -> Result<(), AssemblyError<I>> {
        AssemblyError::assert_length(&dir, 3)?;

        let rd = self.get_register_at(&dir, 0)?;
        let rs1 = self.get_register_at(&dir, 1)?;
        let rs2 = self.get_register_at(&dir, 2)?;

        write_inst32(
            &mut self.w,
            op | with_rd(rd) | with_rs1(rs1) | with_rs2(rs2),
        )?;
        self.offset += 4;

        Ok(())
    }

    fn reg_imm(&mut self, dir: Directive<I>, op: u32) -> Result<(), AssemblyError<I>> {
        AssemblyError::assert_length(&dir, 3)?;

        let rd = self.get_register_at(&dir, 0)?;
        let rs = self.get_register_at(&dir, 1)?;
        let imm = self.get_immediate_at(&dir, 2)?;

        write_inst32(
            &mut self.w,
            op | with_rd(rd) | with_rs1(rs) | with_iimm(imm.unsigned().expect("overflow") as u32), // TODO bounds checking
        )?;
        self.offset += 4;

        Ok(())
    }

    fn get_register_at(
        &self,
        dir: &Directive<I>,
        arg_index: usize,
    ) -> Result<u32, AssemblyError<I>> {
        let name = dir.args[arg_index]
            .word()
            .ok_or_else(|| AssemblyError::WrongArgument {
                dir: dir.clone(),
                arg_index,
                want: "register",
            })?;

        register_from_str_abi(name.as_str()).ok_or_else(|| AssemblyError::UnknownRegisterName {
            dir: dir.clone(),
            arg_index,
        })
    }

    fn get_address_at(
        &self,
        dir: &Directive<I>,
        arg_index: usize,
    ) -> Result<Option<u64>, AssemblyError<I>> {
        let name = dir.args[arg_index]
            .word()
            .ok_or_else(|| AssemblyError::WrongArgument {
                dir: dir.clone(),
                arg_index,
                want: "address",
            })?;

        Ok(self.symbol_table.get(name.as_str()).copied())
    }

    fn get_immediate_at(
        &self,
        dir: &Directive<I>,
        arg_index: usize,
    ) -> Result<Integer, AssemblyError<I>> {
        dir.args[arg_index]
            .int()
            .ok_or_else(|| AssemblyError::WrongArgument {
                dir: dir.clone(),
                arg_index,
                want: "immediate",
            })
    }
}

#[cfg(test)]
mod tests {
    use nom::AsBytes;

    use super::Assembler;
    use crate::parser::dir;

    #[test]
    fn test_branch() {
        // beq a0, a1, _start
        let mut res = Vec::new();
        let mut asm = Assembler::new(&mut res);
        asm.symbol_table.insert("_start".to_owned(), 0);
        asm.write_inst(dir!(beq a0, a1, _start)).unwrap();
        assert_eq!(&[0x63, 0x00, 0xB5, 0x00], res.as_bytes());

        // add t5, a0, s3
        let mut res = Vec::new();
        let mut asm = Assembler::new(&mut res);
        asm.write_inst(dir!(add t5, a0, s3)).unwrap();
        assert_eq!(&[0x33, 0x0F, 0x35, 0x01], res.as_bytes());

        // addi x31, x31, 0x7FF
        let mut res = Vec::new();
        let mut asm = Assembler::new(&mut res);
        asm.write_inst(dir!(addi x31, x31, 0x7FF)).unwrap();
        assert_eq!(&[0x93, 0x8F, 0xFF, 0x7F], res.as_bytes());

        // addi x0, x31, 0x00
        let mut res = Vec::new();
        let mut asm = Assembler::new(&mut res);
        asm.write_inst(dir!(addi x0, x31, 0x00)).unwrap();
        assert_eq!(&[0x13, 0x80, 0x0F, 0x00], res.as_bytes());
    }
}
