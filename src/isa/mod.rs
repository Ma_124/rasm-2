#![allow(dead_code)]

mod assembler;
mod registers;

use std::io::{self, Read, Write};

use crate::parser::Directive;

pub type Inst = u32;

#[derive(Debug)]
pub enum AssemblyError<I> {
    Io(io::Error),
    UnknownInstruction {
        dir: Directive<I>,
    },
    WrongArgumentCount {
        dir: Directive<I>,
        got: usize,
        want: usize,
    },
    WrongArgument {
        dir: Directive<I>,
        arg_index: usize,
        want: &'static str,
    },
    UnknownRegisterName {
        dir: Directive<I>,
        arg_index: usize,
    },
}

impl<I: Clone> AssemblyError<I> {
    pub fn assert_length(dir: &Directive<I>, count: usize) -> Result<(), AssemblyError<I>> {
        if dir.args.len() != count {
            Err(AssemblyError::WrongArgumentCount {
                got: dir.args.len(),
                want: count,
                dir: dir.clone(),
            })
        } else {
            Ok(())
        }
    }
}

impl<I> From<io::Error> for AssemblyError<I> {
    fn from(e: io::Error) -> Self {
        Self::Io(e)
    }
}

// RV.V1: 1.5 Base Instruction-Length Encoding
pub fn read_inst32<R: Read>(mut r: R) -> Result<Inst, io::Error> {
    let mut first = [0u8; 2];
    r.read_exact(&mut first)?;
    if first[0] & 0b11 != 0b11 {
        Ok(u16::from_le_bytes(first) as u32)
    } else {
        let mut second = [0u8; 2];
        r.read_exact(&mut second)?;
        Ok((u16::from_le_bytes(first) as u32) | (u16::from_le_bytes(second) as u32) << 16)
    }
}

// RV.V1: 1.5 Base Instruction-Length Encoding
pub fn write_inst32<W: Write>(mut w: W, i: Inst) -> Result<(), io::Error> {
    w.write_all(&(i as u16).to_le_bytes())?;
    if !is_compressed(i) {
        w.write_all(&((i >> 16) as u16).to_le_bytes())?;
    }
    Ok(())
}

// RV.V1: 1.5 Base Instruction-Length Encoding
#[inline]
pub fn is_compressed(i: Inst) -> bool {
    i & 0b11 != 0b11
}

macro_rules! enc_fns {
    ($($get_ident:ident $with_ident:ident $off:literal $len:literal);* $(;)?) => {
        $(
            #[inline]
            #[allow(clippy::identity_op)]
            pub fn $get_ident(i: Inst) -> u32 {
                (i & ((1 << $len) - 1) << $off) >> $off
            }

            #[inline]
            #[allow(clippy::identity_op)]
            pub fn $with_ident(u: u32) -> Inst {
                let u0 = u & ((1 << $len) - 1);
                debug_assert_eq!(u0, u, "overflow");
                u << $off
            }
        )*
    };
}

macro_rules! imm_fns {
    ($($get_ident:ident $with_ident:ident $($iff: literal : $off:literal $len:literal),*);* $(;)?) => {
        $(
            #[inline]
            #[allow(clippy::identity_op)]
            pub(crate) fn $get_ident(i: Inst) -> u32 {
                0 $( | ((i & ((1 << $len) - 1) << $off) >> $off) << $iff )*
            }

            #[inline]
            #[allow(clippy::identity_op)]
            pub(crate) fn $with_ident(i: u32) -> Inst {
                // TODO sign extension
                0 $( | ((i & ((1 << $len) - 1) << $iff) >> $iff) << $off )*
            }
        )*
    };
}

// RV.V1: 2.3. Immediate Encoding Variants, Fig. 2.3
enc_fns!(
    //                    off len
    get_opcode with_opcode  0  6;
    get_rd     with_rd      7  5;
    get_funct3 with_funct3 12  3;
    get_rs1    with_rs1    15  5;
    get_rs2    with_rs2    20  5;
    get_funct7 with_funct7 25  7;
);

// RV.V1: 2.3. Immediate Encoding Variants, Fig. 2.3
imm_fns!(
    //                iff off len
    get_iimm with_iimm  0: 20 12;
    get_simm with_simm  0:  7  5,  5: 25 7;
    get_bimm with_bimm  1:  8  4,  5: 25 6, 11:  7 1, 12: 31 1;
    get_uimm with_uimm 12: 12 20;
    get_jimm with_jimm  1: 21 10, 11: 20 1, 12: 12 8, 20: 31 1;
);

#[cfg(test)]
mod tests {
    use nom::AsBytes;

    use crate::isa::{
        get_funct3, get_funct7, get_iimm, get_opcode, get_rd, get_rs1, get_rs2, get_uimm,
        read_inst32, with_funct3, with_funct7, with_iimm, with_opcode, with_rd, with_rs1, with_rs2,
        with_uimm, write_inst32,
    };

    macro_rules! test_inst {
        ( $($byte:literal),+ => $($field:ident = $val:expr),* ) => {
            ::paste::paste! {
                let i = read_inst32(&mut [$($byte),+].as_ref()).unwrap();
                $(
                    assert_eq!($val, [< get_ $field >](i), stringify!($field));
                )*

                let mut res = Vec::new();
                write_inst32(
                    &mut res,
                    0u32 $( | [< with_ $field >]($val) )*
                )
                .unwrap();
                assert_eq!(&[$($byte),+], res.as_bytes());
            }
        };
    }

    #[test]
    fn test_inst32() {
        // EBREAK
        assert_eq!(
            "00000000000100000000000001110011",
            &format!(
                "{:032b}",
                read_inst32(&mut [0x73, 0x00, 0x10, 0x00].as_ref()).unwrap()
            )
        );
        // C.MV a1, a2; C.ADD a3, a4
        assert_eq!(
            "1000010110110010",
            &format!(
                "{:16b}",
                read_inst32(&mut [0xB2, 0x85, 0xBA, 0x96].as_ref()).unwrap()
            )
        );
        // AMOMAXU.W.aq a0, a1, (a2)
        let i = read_inst32(&mut [0x2F, 0x25, 0xB6, 0xE4].as_ref()).unwrap();
        const OP_AMO: u32 = 0b0010_1111;
        const WIDTH: u32 = 0b010;
        const AMOMAXU: u32 = 0b0111_0010;
        assert_eq!(OP_AMO, get_opcode(i));
        assert_eq!(10, get_rd(i));
        assert_eq!(WIDTH, get_funct3(i));
        assert_eq!(12, get_rs1(i)); // addr
        assert_eq!(11, get_rs2(i)); // src
        assert_eq!(AMOMAXU, get_funct7(i));
        // reverse
        let mut res = Vec::new();
        write_inst32(
            &mut res,
            with_opcode(OP_AMO)
                | with_rd(10)
                | with_funct3(WIDTH)
                | with_rs1(12)
                | with_rs2(11)
                | with_funct7(AMOMAXU),
        )
        .unwrap();
        assert_eq!(&[0x2F, 0x25, 0xB6, 0xE4], res.as_bytes());
        // AMOMAXU.W.aq a0, a1, (a2)
        test_inst!(0x2F, 0x25, 0xB6, 0xE4 => opcode = OP_AMO, rd = 10, funct3 = WIDTH, rs1 = 12, rs2 = 11, funct7 = AMOMAXU);
        // LUI a0, 0xABCDE
        test_inst!(0x37, 0xE5, 0xCD, 0xAB => opcode = 0b0011_0111, rd = 10, uimm = 0xABCD_E000);
        // ADDI a0, a1, 0x7FF
        test_inst!(0x13, 0x85, 0xF5, 0x7F => opcode = 0b0001_0011, rd = 10, rs1 = 11, iimm = 0x7FF);
        // SRA a0, a1, a2
        test_inst!(0x33, 0xD5, 0xC5, 0x40 => opcode = 0b0011_0011, rd = 10, funct3 = 0b0101, rs1 = 11, rs2 = 12, funct7 = 0b0010_0000);
    }
}
