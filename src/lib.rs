#[cfg(feature = "isa")]
pub mod isa;
pub mod parser;
#[cfg(feature = "vm")]
pub mod vm;

#[cfg(all(feature = "color", not(windows)))]
#[allow(dead_code)]
pub(crate) mod color {
    pub fn info(s: &str) -> String {
        format!("\x1b[34m{}\x1b[39m", s)
    }
    pub fn warn(s: &str) -> String {
        format!("\x1b[33m{}\x1b[39m", s)
    }
    pub fn error(s: &str) -> String {
        format!("\x1b[31m{}\x1b[39m", s)
    }
}

#[cfg(not(all(feature = "color", not(windows))))]
#[allow(dead_code)]
pub(crate) mod color {
    pub fn info(s: &str) -> String {
        s.to_owned()
    }
    pub fn warn(s: &str) -> String {
        s.to_owned()
    }
    pub fn error(s: &str) -> String {
        s.to_owned()
    }
}
